const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const path = require('path');
const favicon = require('serve-favicon');
const helmet = require('helmet');
const fs = require('fs');
const app = express();
const mongoose = require('mongoose');
require('./models/comment.model');

const privateKey  = fs.readFileSync('./certificates/key.pem', 'utf8');
const certificate = fs.readFileSync('./certificates/cert.pem', 'utf8');
const credentials = {key: privateKey, cert: certificate};

const authRouter = require('./routes/auth.route');
const lessonRouter = require('./routes/lesson.route');
const mainRouter = require('./routes/main.route');
const profileRouter = require('./routes/profile.route');
const teacherRouter = require('./routes/teacher.route');

app.use(helmet());
app.use(favicon(path.join(__dirname, 'public/images/ico', 'favicon.ico')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cookieParser('anything'));

app.use(session({
  secret: 'anything', 
  resave: true, 
  saveUninitialized: true,  
  cookie : { 
    secure : false, 
    maxAge : (4 * 60 * 60 * 1000) }
  })
);

const db_url = 'mongodb://erudit:pass123@ds159707.mlab.com:59707/eruditio';
const mongoDB = process.env.MONGODB_URI || db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/auth', authRouter);
app.use('/lessons', lessonRouter);
app.use('/', mainRouter);
app.use('/profile', profileRouter);
app.use('/teachers', teacherRouter);

const port = process.env.PORT || 443;
const server = https.createServer(credentials, app);
var io = require('socket.io').listen(server)
var socketio = require('./src/socketio')(io)




server.listen(port, function() {
  console.log('Eruditio running on port: ' + port + '.');
});


var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80);