
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user.model');
const mongoose = require('mongoose');

module.exports = function(passport) {
    passport.serializeUser(function(user, done){
        done(null, user);
    });
      
    passport.deserializeUser(function(user, done){
        done(null, user)
    });

    passport.use(new LocalStrategy(
        function(username, password, done) {
        User.findOne({ username: username }, function(err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            if (user.password != password) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);
          });
        }
    ));
};   
