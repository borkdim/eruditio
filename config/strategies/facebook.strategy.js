var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;


module.exports = function() {
    passport.use(new FacebookStrategy({
        clientID: 265044360552125,
        clientSecret: "520659ffafbc9db72a7021a47ee7a039",
        callbackURL: "https://localhost/auth/facebook/callback",
        profileFields: ['id', 'name', 'emails']
    }));
/*
        Account.findOne({'fb.id': profile.id }, function (err, user) {
            if (err) {
                return cb(err);
            }
            if(user) {
                return cb(null,user);
            }
            else {
               var newAcc = new Account({
                    fb: {
                        id: profile.id,
                        access_token: accessToken
                    },
                    firstName: profile.name.givenName,
                    lastName: profile.name.familyName,
                    email: profile.emails[0].value,
                    imageURL: "https://graph.facebook.com/" + profile.id + "/picture?type=large",
                });
                newAcc.save(function(err) {
                    if(err) throw err;
                });
                return cb(null, newAcc);   
                
            }
        });
*/