const express = require('express');
const authRouter = express.Router();
const passport = require('passport');
const User = require('../models/user.model');

authRouter.get('/register', function (req, res) {
    res.render('register');
});

//worked on, isStudent ne raboti
authRouter.post('/register', function (req, res) {
    if (req.body.password != req.body.password_confirmation) {
        return res.json({
            user: null,
            message: "Passwords don't match!",
            backURL: "/"
        });
    } else {
        User
        .findOne({email: req.body.email})
        .exec(function(err, result){
            if(err)
            {
                console.log('Error Auth Router at Register' + err);
                res.redirect('/');
            }
            if (result === null) {
                let user = new User(
                    {
                        email: req.body.email,
                        firstName: req.body.first_name,
                        lastName: req.body.last_name,
                        password: req.body.password,
                        username: req.body.display_name,
                        isStudent: req.body.profile_type == 'true' ? true : false
                    }
                );
                
                user.save(function (err) {
                    if (err) {
                        console.log(err);
                        res.redirect('/auth/register');
                    }
                    req.login(user, function (err) {
                        if (err){
                            //handle
                        } else {
                            res.redirect('/profile/edit');
                        }
                    })
                })
            } else 
                res.send('ima');
                //handle
        });
    }
});

authRouter.get('/login', function (req, res) {
    res.render('login');
});

//flashovete da se opravqt
authRouter.post('/login', passport.authenticate('local', { 
        successRedirect: '/teachers', 
        failureRedirect: '/auth/login'
        // failureFlash: true 
    })
);

authRouter.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

authRouter.get('/facebook',
    passport.authenticate('facebook', {
        scope: 'email'
    })
);
authRouter.get('/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect: '/',
        failureRedirect: '/'
    })

);
authRouter.get('/google',
    passport.authenticate('google', {
        scope: ['https://www.googleapis.com/auth/userinfo.profile',
				 'https://www.googleapis.com/auth/userinfo.email']
    })

);
authRouter.get('/google/callback',
    passport.authenticate('google', {
        successRedirect: '/profile',
        failureRedirect: '/'
    })
);

module.exports = authRouter;