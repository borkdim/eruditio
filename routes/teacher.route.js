const express = require('express');
const teacherRouter = express.Router();
const User = require('../models/user.model');

//nqma tursene we tapanr + paging + izprashtane na lichno saobshtenie
teacherRouter.get('/', function(req,res){
	if(!req.user) {
        return res.redirect('/');
    } 	
    let query = {isStudent: false};
    User
    .find(query)
    .exec(function(err, teachers){
        if(err){
            console.log(err);
            //handle
        }
        else{
            let users=[];
		    teachers.forEach(function(teacher) {
		  	users.push({
		  		id: teacher.id,
		  		firstName: teacher.firstName,
		  		lastName: teacher.lastName,
		  		avatar: teacher.avatar
		  	});
		  });
		  res.render('teachers', {teachers: users, user: req.user});
        }
    })
});

module.exports = teacherRouter;