const express = require('express');
const lessonRouter = express.Router();
const User = require('../models/user.model');
const PublicLesson = require('../models/publicLesson.model');
const PrivateLesson = require('../models/privateLesson.model');
const mongoose = require('mongoose');

//worked on
lessonRouter.get('/', function (req, res) {
    PublicLesson
        .find()
        .exec(function (err, results) {
            if (err) {
                console.log(err);
                //handle
            }

            let lessons = [];
            for (let i = 0; i < results.length; i++) {
                lessons.push({
                    id: results[i].id,
                    name: results[i].name,
                    date: results[i].date,
                    subject: results[i].subject
                });
            }

            res.render('lessons', {
                user: req.user,
                lessons: lessons
            });
        });
});

//done
lessonRouter.get('/post', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }
    res.render('lesson-info');
});

lessonRouter.get('/live/:lessonId', function (req, res) {
    if (!req.user) {
        return res.redirect('/');
    }

    let lessonid = req.params.lessonId;
    PrivateLesson
        .findById(lessonid)
        .populate('student')
        .populate('teacher')
        .exec(function (err, result) {
            if (err) {
                console.log(err);
            }
            console.log(result);
            let studentAvatar = result.student.avatar || { _url: '/images/default.jpg' };
            let teacherAvatar = result.teacher.avatar || { _url: '/images/default.jpg' };
            let theLesson = {
                id: result._id,
                teacher: result.teacher._id,
                teacherName: result.teacher.firstName,
                teacherLastName: result.teacher.lastName,
                teacherAvatar: teacherAvatar,
                student: result.student._id,
                studentName: result.student.firstName,
                studentLastName: result.student.lastName,
                studentAvatar: studentAvatar,
                date: result.date
            }

            res.render('live-lesson', {
                user: req.user,
                lesson: theLesson
            });
        })
});

//worked on VRUSHTAM REQ.USER???
lessonRouter.get('/public/live/:lessonId', function (req, res) {
    if (!req.user) {
        return res.redirect('/');
    }

    var lessonId = req.params.lessonId;
    PublicLesson
        .findById(lessonId)
        .populate('teacher')
        .exec(function (err, result) {
            if (err) {
                console.log(err);
                //handle
            }
            console.log(result);
            res.render('live-public-lesson', {
                user: req.user,
                lesson: result
            });
        });


});

//workedOn
lessonRouter.post('/post', function (req, res) {
    if (!req.user) {
        return res.redirect('/');
    }

    let dateOfLesson = new Date(req.body.date);
    dateOfLesson.setHours(req.body.time.substr(0, 2));
    dateOfLesson.setMinutes(req.body.time.substr(3, 5));

    let pLesson = new PublicLesson(
        {
            name: req.body.name,
            date: dateOfLesson,
            info: req.body.addInfo,
            teacher: req.user._id,
            subject: req.body.subject
        }
    );

    pLesson.save(function (err) {
        if (err) {
            console.log(err);
            //handle
        }
        req.login(pLesson, function (err) {
            if (err) {
                //handle
                console.log(err);
            } else {
                res.redirect('/');
            }
        })
    })
});


module.exports = lessonRouter;
