const express = require('express');
const mainRouter = express.Router();
const User = require('../models/user.model');

mainRouter.get('/', function(req, res, next){
    User
    .find({isStudent: false})
    .limit(4)
    .exec(function(err, result){
        if(err){
            console.log(error);
        }
        else{
            let teachers = [], i = 0;
            for(i; i < result.length; i++){
                teachers.push({
                    firstName: result[i].firstName,
                    lastName: result[i].lastName,
                    avatar: result[i].avatar
                });
            }
            
            res.render('index', {
                user:req.user,
                teachers: teachers
            });
        }
    });
});

module.exports = mainRouter;
