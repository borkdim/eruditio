const express = require('express');
const profileRouter = express.Router();
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const User = require('../models/user.model');
const Message = require('../models/message.model');
const LessonRequest = require('../models/lessonRequest.model');
const PrivateLesson = require('../models/privateLesson.model');


profileRouter.get('/edit', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    } else {
        if (req.user.isStudent) {
            res.render('student-info', {
                user: req.user
            });
        } else {
            res.render('teacher-info', {
                user: req.user
            });
        }
    }
});

profileRouter.post('/edit', upload.single('image'), function (req, res) {

    if (!req.user) {
        return res.redirect('/');
    } else {
        let query = { _id: req.user._id };
        User.findOneAndUpdate(query, {
            forMe: req.body.forMe,
            school: req.body.school,
            location: req.body.location,
            avatar: req.file.buffer.toString('base64'),
            experience: !req.user.isStudent ? req.body.experience : null,
            subject: !req.user.isStudent ? req.body.subject : null,
        }, function (err) {
            if (err) {
                //handle
                console.log(err);
            } else {
                res.redirect('/profile/id/' + req.user._id);
            }
        })
    }
});

profileRouter.get('/messages', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }
    let messagesInfo = [];
    let uniqueNames = [];

    Message
        .find({
            $or: [{ 'sender': req.user._id }, { 'receiver': req.user._id }]
        })
        .populate('receiver')
        .populate('sender')
        .sort({ 'createdAt': 'ascending' })
        .exec(function (err, messages) {
            if (err) {
                console.log(err);
            }

            for (let i = 0; i < messages.length; i++) {
                let isReceiver = messages[i].sender._id != req.user._id;
                let sender = messages[i].sender;

                let receiver = messages[i].receiver;
                let messageObject = {
                    sender: isReceiver ? sender.firstName : receiver.firstName,
                    realName: sender.firstName,
                    lastName: isReceiver ? sender.lastName : receiver.lastName,
                    realLastName: sender.lastName,
                    username: isReceiver ? sender.username : receiver.username,
                    isStudent: sender.isStudent,
                    realAvatar: sender.avatar,
                    issuer: isReceiver ? false : true,
                    senderImage: isReceiver ? sender.avatar : receiver.avatar,
                    senderID: isReceiver ? sender._id : receiver._id,
                    body: messages[i].body,
                    date: messages[i].createdAt
                };
                messagesInfo.push(messageObject);
                // console.log(messageObject.sender);
                // console.log(messageObject.receiver);
                // console.log(messageObject.body);
            }
            let flags = [];


            // Think for better implementation
            for (let i = 0; i < messagesInfo.length; i++) {
                if (flags[messagesInfo[i].sender]) continue;
                flags[messagesInfo[i].sender] = true;
                uniqueNames.push({
                    sender: messagesInfo[i].sender,
                    senderID: messagesInfo[i].senderID,
                    username: messagesInfo[i].username,
                    senderImage: messagesInfo[i].senderImage,
                    lastName: messagesInfo[i].lastName,
                    isStudent: messagesInfo[i].isStudent
                });
            }
            res.render('messages', {
                user: req.user,
                messages: messagesInfo,
                myChatFriends: uniqueNames
            });
        });
});

//gotovo
profileRouter.get('/lessons', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/auth/login');
    }

    var pastLessons = [];
    var futureLessons = [];
    PrivateLesson.find(
        { $or: [{ 'teacher': req.user._id }, { 'student': req.user._id }] }
    )
        .populate('teacher')
        .exec(function (err, results) {
            results.forEach(function (result)  {
                if (result.date > new Date()) {
                    futureLessons.push({
                        id: result._id,
                        teacherName: result.teacher.firstName,
                        teacherLastName: result.teacher.lastName,
                        date: result.date,
                        subject: result.teacher.subject,
                        isPrivate: true
                    });
                } else {
                    pastLessons.push({
                        id: result._id,
                        teacherName: result.teacher.firstName,
                        teacherLastName: result.teacher.lastName,
                        date: result.date,
                        subject: result.teacher.subject,
                        isPrivate: true
                    })
                }
            })

            res.render('my-lessons', {
                user: req.user,
                pastLessons: pastLessons,
                futureLessons: futureLessons
            });
        });
});

//gotov, comment ako ima vreme
profileRouter.get('/id/:profileId', function (req, res, next) {
    User.findById(req.params.profileId)
        .populate('comments')
        .exec(function (err, user) {
            if (err) {
                console.log(err);
                //handle
            } else {
                let comments = [];
                /*for( var i = 0; i < user.comments.length; i++) {
                    comments.push({
                        body: user.comments[i].get('body'),
                        sender: {
                            id: user.comments[i].get('sender').id,
                            firstName: user.comments[i].get('sender').get('firstName'),
                            lastName: user.comments[i].get('sender').get('lastName'),
                            status: user.comments[i].get('sender').get('isStudent') ? 'Ученик' : 'Учител'
                        }
                    });
                } */

                console.log(user);
                let account = {
                    id: user._id,
                    username: user.username,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    avatar: user.avatar,
                    forMe: user.forMe,
                    subject: user.subject || null,
                    experience: user.experience || null,
                    location: user.location,
                    school: user.school,
                    comments: comments
                };

                if (!user.isStudent) {
                    res.render('teacher-profile', {
                        user: req.user,
                        teacher: account
                    });
                } else {
                    res.render('student-profile', {
                        user: req.user,
                        student: account
                    });
                }
            }
        });
});

profileRouter.post('/id/:profileId/addComment', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/auth/login');
    }

});

//raboti, avatar da se oprai
profileRouter.get('/id/:profileId/request', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }

    User
        .findById(req.params.profileId)
        .exec(function (err, result) {
            if (err) {
                console.log(err);
            }

            var teacher = {
                id: result.id,
                firstName: result.firstName,
                lastName: result.lastName,
                avatar: result.avatar,
                forMe: result.forMe,
                subject: result.subject,
                location: result.location,
            };

            res.render('request', {
                user: req.user,
                teacher: teacher
            });
        });
});

//gotov
profileRouter.post('/id/:profileId/request', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }

    if (req.params.profileId == req.user.objectId) {
        return res.render('error.ejs', {
            error: {
                message: "Не можеш да кандидатстваш за собствения си урок"
            }
        });
    }

    let dateOfLesson = new Date(req.body.date);
    dateOfLesson.setHours(req.body.hour.substr(0, 2));
    dateOfLesson.setMinutes(req.body.hour.substr(3, 5));

    let lRequest = new LessonRequest(
        {
            date: dateOfLesson,
            teacher: req.params.profileId,
            student: req.user._id,
            quote: req.body.addInfo
        }
    );

    lRequest.save(function (err) {
        if (err) {
            console.log(err);
            //handle
        }
        return res.redirect('/profile/id/' + req.params.profileId);
    });
});

//gotov, avatar
profileRouter.get('/requests', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }

    if (req.user.isStudent) {
        return res.redirect('/');
    }

    let requests = [];

    LessonRequest.find({ teacher: req.user._id })
        .populate('student')
        .exec(function (err, results) {
            if (err) {
                console.log(err)
            }

            results.forEach((result) => {
                requests.push({
                    id: result._id,
                    firstName: result.student.firstName,
                    lastName: result.student.lastName,
                    avatar: result.student.avatar,
                    username: result.student.username,
                    date: result.date,
                    quote: result.quote
                });
            });

            res.render('requests', {
                user: req.user,
                requests: requests
            });
        })
});

//gotov
profileRouter.get('/requests/:requestId/accept', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }


    LessonRequest.findById(req.params.requestId)
        .exec(function (err, lRequest) {
            if (err) {
                console.log(err)
            }

            let pLesson = new PrivateLesson(
                {
                    date: lRequest.date,
                    teacher: lRequest.teacher,
                    student: lRequest.student,
                }
            );

            pLesson.save(function (err) {
                if (err) {
                    console.log(err);
                }
                else {
                    lRequest.remove();
                    return res.redirect('/profile/requests');
                }
            });

        })
});

//gotov
profileRouter.get('/requests/:requestId/reject', function (req, res, next) {
    if (!req.user) {
        return res.redirect('/');
    }
    LessonRequest.findByIdAndDelete(req.params.requestId)
        .exec(function (err) {
            if (err) {
                console.log(err);
            }
            return res.redirect('/profile/requests');
        });
});

module.exports = profileRouter;
