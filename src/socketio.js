var _ = require('underscore');
const Message = require('../models/message.model');
exports = module.exports = function (io) {
  var clients = [];
  io.on('connection', function (socket) {

    var handshakeData = socket.request;
    if (handshakeData._query['doo']) {
      clients.push({ id: socket.id, oid: handshakeData._query['foo'], avatar: handshakeData._query['avatar'], room: handshakeData._query['doo'], name: handshakeData._query['name'], lastName: handshakeData._query['lastName'] });
    }
    else {
      clients.push({ id: socket.id, oid: handshakeData._query['foo'] });
    }

    if (handshakeData._query['doo']) {
      socket.join(handshakeData._query['doo']);
      socket.to(handshakeData._query['doo']).emit('addUser', {
        lastName: handshakeData._query['lastName'],
        avatar: handshakeData._query['avatar'],
        name: handshakeData._query['name']
      });

    }
    // da se proveri dali si lognat kato otvarqsh kvoto i da bilo BOBINAAAAss

    //todo - make only one function (sendingMessage)
    socket.on('hello', function (data) {
      let message = new Message({
        body: data.body,
        sender: data.sender,
        receiver: data.receiver
      });
      
      message.save(function (err) {
        if (err) {
          console.log('Couldnt send message at hello');
          console.log(err);
        } else {
          console.log('Send message successfully at hello');
        }
      });
      
      let socketMessage = {
        body: data.body,
        sender: handshakeData._query['foo'],
        date: Date.now(),
        firstName: data.name,
        lastName: data.lastName,
        avatar: data.avatar,
        id: data.id
      };

      io.to(data.receiver).emit('pm', socketMessage);

    });

    socket.on('sendingMessage', function (data) {
      for (var i = 0; i < clients.length; i++) {
        if (clients[i].oid == data.toWho) {
          io.to(clients[i].id).emit('pm', {
            body: data.body,
            sender: handshakeData._query['foo'],
            date: Date.now(),
            firstName: data.name,
            lastName: data.lastName,
            avatar: data.avatar
          });
        }
      }


      let message = new Message({
        body: data.body,
        sender: handshakeData._query['foo'],
        receiver: data.toWho
      });
      message.save(function (err) {
        if (err) {
          console.log('Couldnt send message at sendingMessage');
          console.log(error);
        } else {
          console.log('Send message successfully at sendingMessage');
        }
      })
    });

    socket.on('publicMessage', function (data) {
      console.log(data.room);
      console.log(data.body);
      socket.to(data.room).emit('public', {
        body: data.body,
        lastName: data.lastName,
        avatar: data.avatar,
        name: data.name,
        id: data.id
      });
    });

    socket.on('privateMessage', function (data) {
      for (var i = 0; i < clients.length; i++) {

        if (clients[i].oid == data.id) {

          io.to(clients[i].id).emit('private', {
            body: data.body,
            lastName: data.lastName,
            avatar: data.avatar,
            name: data.name
          });
        }
      }
      for (var i = 0; i < clients.length; i++) {

        if (clients[i].oid == data.student) {

          io.to(clients[i].id).emit('private', {
            body: data.body,
            lastName: data.lastName,
            avatar: data.avatar,
            name: data.name
          });
        }
      }
    });

    socket.on('oneTimeCheck', function (data) {
      var clien = io.sockets.adapter.rooms[data.room].sockets;
      var toAdd = [];
      var numClients = (typeof clien !== 'undefined') ? Object.keys(clien).length : 0;
      console.log(numClients);
      for (var clientId in clien) {
        var clientSocket = io.sockets.connected[clientId];
        console.log(clientSocket.id);
        for (var i = 0; i < clients.length; i++) {
          if (clients[i].id == clientSocket.id) {
            toAdd.push({
              oid: clients[i].oid,
              name: clients[i].name,
              lastName: clients[i].lastName,
              avatar: clients[i].avatar
            });
          }
        }
      }

      toAdd = _.uniq(toAdd, true /* array already sorted */, function (item) {
        return item.id;
      });

      socket.to(data.room).emit('check', {
        data: toAdd
      });

    });

    socket.on('disconnect', function () {


      console.log('Got disconnect!');
      for (var i = 0; i < clients.length; i++) {
        if (clients[i].id == socket.id) {
          socket.to(clients[i].room).emit('removeUser', {
            id: clients[i]
          });
          clients.splice(i, 1);
        }
      }
    });



  });

}