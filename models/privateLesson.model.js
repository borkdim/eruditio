const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let privateLessonSchema = new Schema({
    date: {type: Date},
    teacher: { type: Schema.Types.ObjectId, ref: 'User' },
    student: { type: Schema.Types.ObjectId, ref: 'User' },
});


module.exports = mongoose.model('privateLesson', privateLessonSchema);