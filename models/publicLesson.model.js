const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let publicLessonSchema = new Schema({
    name: {type: String},
    date: {type: Date},
    teacher: { type: Schema.Types.ObjectId, ref: 'User' },
    info: {type: String},
    subject: {type: String}
});


module.exports = mongoose.model('publicLesson', publicLessonSchema);