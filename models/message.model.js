const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MessageSchema = new Schema({
    receiver: { type: Schema.Types.ObjectId, ref: 'User' },
    sender: { type: Schema.Types.ObjectId, ref: 'User' },
    body: {type: String, required: true},
    createdAt: {type: Date, default: Date.now}
});


module.exports = mongoose.model('Message', MessageSchema);