const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let lessonRequestSchema = new Schema({
    date: {type: Date},
    teacher: { type: Schema.Types.ObjectId, ref: 'User' },
    student: { type: Schema.Types.ObjectId, ref: 'User' },
    quote: {type: String}
});


module.exports = mongoose.model('lessonRequest', lessonRequestSchema);